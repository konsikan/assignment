package reportCard;

import java.util.Scanner;

/**
 * Teacher panel start from here.
 * 
 * @author J.Konsikan
 * @version 1.0
 *
 */
public class TeacherPanel {
	public void start() {
		PrintingMessages message = new PrintingMessages();
		LogInSignIn loginOrSignIn = new LogInSignIn();
		message.panels(1);
		// TEACHER PANEL
		message.logInOrSignIn(1);
		// 1-LOGIN | 2-CREAT NEW ACCOUNT
		Scanner input = new Scanner(System.in);
		int correction = 1;
		while (correction == 1) {
			message.type();
			int type = input.nextInt();
			System.out.println();
			if (type == 1) {
				String username = "TE01";
				String password = "TEBCAS01";
				loginOrSignIn.logIn(username, password);
				break;
			} else if (type == 2) {
				message.teacherPanel(1);
				// YOU NEED ADMIN PERMISION TO CREAT!
				String username = "ADMIN01";
				String password = "ADMINBCAS01";
				loginOrSignIn.logIn(username, password);
				message.teacherPanel(2);
				// ACCESS GRANDED
				loginOrSignIn.signIn();
				break;
			} else {
				correction = 1;
				message.incorrectValue();
			}
		}
	}
}
